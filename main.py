from PyQt5.QtWidgets import QApplication
import sys
from pyads import ADSError

from twinpy.twincat import SimulinkModel, TwincatConnection

from ui.window import WE2GUI


if __name__ == "__main__":

    app = QApplication(sys.argv)

    models = {
        "actuator": SimulinkModel(0x01010020, "we2_actuator_model"),
        "controller": SimulinkModel(0x01010030, "we2_controller_model"),
    }

    try:
        connection = TwincatConnection()
        for name, model in models.items():
            model.connect_to_twincat(connection)

    except (ADSError, RuntimeError) as err:
        connection = None
        print("Not connected to TwinCAT, continuing with disabled GUI -", err)

    gui = WE2GUI(**models)

    app.exec()
