# WR General GUI

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/ctw-bw/wr-general-gui/badge)](https://www.codefactor.io/repository/bitbucket/ctw-bw/wr-general-gui)

This GUI should serve as a portable application between wearable robotics projects. The main purpose is to provide debug and maintenance info.

You could run your customized project with this GUI alongside your own, or you could extend this GUI instead.
