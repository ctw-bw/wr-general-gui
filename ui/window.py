from PyQt5.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QGroupBox,
)
from typing import Optional

from twinpy.twincat import SimulinkModel
from twinpy.ui import TcWidget, BaseGUI, TcRadioButtonGroupBox, TcCheckBox


# Change TwinPy defaults
TcWidget.DEFAULT_EVENT_TYPE = TcWidget.EVENT_TIMER
TcWidget.DEFAULT_UPDATE_FREQ = 10.0


class WE2GUI(BaseGUI):
    """WE2 GUI

    Made for general purpose, for the WE2 or other WR systems.

    The GUI is made up of tabs. Each tab forms its own class.
    """

    JOINTS = ["LHA", "LHF", "LK", "LA", "RHA", "RHF", "RK", "RA"]

    def __init__(
        self,
        actuator: Optional[SimulinkModel] = None,
        controller: Optional[SimulinkModel] = None,
        **kwargs
    ):

        super().__init__(actuator=actuator, controller=controller, **kwargs)

        joint_names = []  # Try to determine the actuator names automatically

        if actuator is not None:
            for name, block in actuator.FromActuators._subblocks.items():
                if block.type.lower() == "subsystem":
                    joint_names.append(name)
        if joint_names:
            WE2GUI.JOINTS = sorted(joint_names)

        if controller is not None and hasattr(controller, "Controller"):
            controller = controller.Controller  # Shorten one block

        self.tab_control = ControlTab(controller)
        if self.tab_control.connected:
            self.tabs.addTab(self.tab_control, "Control")

        self.tab_actuators = ActuatorTab(controller)
        if self.tab_actuators.connected:
            self.tabs.addTab(self.tab_actuators, "Actuators")

        self.tabs.setCurrentWidget(self.tab_control)


class ControlTab(QWidget):
    """Tab to manage general control setting, like switching to minimal impedance."""

    def __init__(self, controller: Optional[SimulinkModel] = None):
        super().__init__()

        layout_main = QVBoxLayout(self)

        self.radio_mode = TcRadioButtonGroupBox(
            title="Control Mode",
            options=[('Position Mode', 1), ('Minimal Impedance', 0)]
        )

        try:
            if controller is not None:
                self.radio_mode.connect_symbol(controller.Settings.EnablePos.Value)
                self.connected = True
        except AttributeError:
            self.connected = False

        layout_main.addWidget(self.radio_mode)


class ActuatorTab(QWidget):
    """Tab for actuator selection."""

    def __init__(self, controller: Optional[SimulinkModel] = None):
        super().__init__()

        layout_main = QVBoxLayout(self)

        groupbox = QGroupBox("Enable/disable actuators")
        layout_group = QVBoxLayout(groupbox)

        self.check_actuators = {joint: TcCheckBox(joint) for joint in WE2GUI.JOINTS}

        try:
            if controller is not None:
                for joint, checkbox in self.check_actuators.items():
                    block = getattr(controller.UseActuators, joint)
                    checkbox.connect_symbol(block.Value)
                self.connected = True
        except AttributeError:
            self.connected = False

        for _joint, checkbox in self.check_actuators.items():
            layout_group.addWidget(checkbox)

        layout_main.addWidget(groupbox)
